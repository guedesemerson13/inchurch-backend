from rest_framework import serializers
from .models import User
from django.utils.crypto import get_random_string
from django.contrib.auth import authenticate


class UserSignupSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=65, min_length=8, write_only=True)

    class Meta:
        model = User
        fields = (
            'id',
            'email',
            'name',
            'password',
            'username',
            'department',
        )
        read_only_fields = [
            'id',
            'username'
        ]

    def validate(self, attrs):
        email = attrs.get('email', '')
        if User.objects.filter(email=email).exists():
            raise serializers.ValidationError(
                {'email': 'Email already exists'})
        return super().validate(attrs)

    def create(self, validated_data):
        if 'department' not in validated_data or not validated_data['department']:
            raise serializers.ValidationError(
                {'department': 'Department is required/Cannot be Null'})

        full_name = validated_data['name']
        name_first = full_name.split()
        random_code = get_random_string(5)

        validated_data['username'] = f'{name_first[0]}_{random_code}'
        return User.objects.create_user(**validated_data)


class UserAuthenticateSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=65, min_length=8, write_only=True)

    class Meta:
        model = User
        fields = (
            'username',
            'password',
        )


class UserListSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'id',
            'name',
        )


class UserRetrieveSerializer(serializers.ModelSerializer):
    department = serializers.CharField(source='department.name', required=False)

    class Meta:
        model = User
        fields = (
            'id',
            'name',
            'email',
            'username',
            'department'
        )


class UserUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'email',
            'name',
            'department',
        )

    def validate(self, attrs):
        user_token = self.context['request'].user
        current_user = self.instance
        if not user_token.is_superuser:
            if user_token.department_id == current_user.department_id:
                pass
            else:
                raise serializers.ValidationError(
                    {'validation': 'You are not a super user/Diferent department instance.'})
        return super().validate(attrs)


class ResetUserPasswordSerializer(serializers.ModelSerializer):
    old_password = serializers.CharField(max_length=65, min_length=8, write_only=True, required=True)

    class Meta:
        model = User
        fields = (
            'id',
            'name',
            'old_password',
            'password',
            'department',
        )

        read_only_fields = [
            'id',
            'username',
            'name',
            'department',
        ]

    def validate(self, attrs):
        username = self.instance.username
        old_password = attrs.get('old_password', '')
        password = attrs.get('password', '')
        if old_password == password:
            raise serializers.ValidationError(
                {'password': 'Set a diferent password'})
        user_authenticate = authenticate(username=username, password=old_password)

        if not user_authenticate:
            user = User.objects.filter(username=username, password=old_password)
        else:
            user = user_authenticate

        if not user:
            raise serializers.ValidationError(
                {'password': 'Wrong old password'})
        return super().validate(attrs)
