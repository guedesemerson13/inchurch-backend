from django.urls import path
from .views import (RegisterUserView,
                    AuthenticateUserView,
                    ListUserProfileView,
                    RetrieveUserProfileView,
                    DeleteUserProfileView,
                    UpdateUserProfile,
                    ChangePassUserView)

app_name = "api-user"

urlpatterns = [
    path('register_user', RegisterUserView.as_view(), name='register_user'),
    path('authenticate_user', AuthenticateUserView.as_view(), name='authenticate_user'),
    path('list_profile', ListUserProfileView.as_view(), name='list_profile'),
    path('retrieve_profile/<int:id>', RetrieveUserProfileView.as_view(), name='retrieve_profile'),
    path('delete_profile/<int:id>', DeleteUserProfileView.as_view(), name='delete_profile'),
    path('update_profile/<int:id>', UpdateUserProfile.as_view(), name='update_profile'),
    path('change_password/<int:id>', ChangePassUserView.as_view(), name='change_password'),
]
