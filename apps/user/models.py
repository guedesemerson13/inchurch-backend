from django.contrib.auth.models import AbstractUser
from django.db import models
from apps.department.models import Department


class User(AbstractUser):
    name = models.CharField(max_length=150,
                            verbose_name='Name',
                            unique=True)
    department = models.ForeignKey(Department, on_delete=models.SET_NULL, null=True)

    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"

    def __str__(self):
        return self.username