from rest_framework.generics import (ListAPIView,
                                     GenericAPIView,
                                     RetrieveAPIView,
                                     DestroyAPIView,
                                     UpdateAPIView)
from .serializers import (DepartmentSerializer,
                          DepartmentUpdateSerializer)
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from .models import Department


class CreateDepartmentView(GenericAPIView):
    serializer_class = DepartmentSerializer
    permission_classes = (IsAdminUser,)

    def post(self, request):
        serializer = DepartmentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ListDepartmentView(ListAPIView):

    serializer_class = DepartmentSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Department.objects.all()


class RetrieveDepartmentView(RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = DepartmentSerializer
    lookup_field = "id"

    def get_queryset(self):
        queryset = Department.objects.filter(id=self.kwargs['id'])
        return queryset


class DeleteDepartmentView(DestroyAPIView):
    permission_classes = (IsAdminUser,)
    lookup_field = "id"

    def get_queryset(self):
        queryset = Department.objects.filter(id=self.kwargs['id'])
        return queryset

    def perform_destroy(self, instance):
        instance.delete()


class UpdateDepartmentView(UpdateAPIView):
    serializer_class = DepartmentUpdateSerializer
    permission_classes = (IsAuthenticated,)
    lookup_field = 'id'

    def get_queryset(self):
        queryset = Department.objects.filter(id=self.kwargs['id'])
        return queryset

    def perform_update(self, serializer):
        serializer.save()