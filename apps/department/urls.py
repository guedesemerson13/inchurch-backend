from django.urls import path
from .views import (CreateDepartmentView,
                    ListDepartmentView,
                    RetrieveDepartmentView,
                    DeleteDepartmentView,
                    UpdateDepartmentView)

app_name = "api-department"

urlpatterns = [
    path('create_department', CreateDepartmentView.as_view(), name='create_department'),
    path('list_department', ListDepartmentView.as_view(), name='list_department'),
    path('retrieve_department/<int:id>', RetrieveDepartmentView.as_view(), name='retrieve_department'),
    path('delete_department/<int:id>', DeleteDepartmentView.as_view(), name='delete_department'),
    path('update_department/<int:id>', UpdateDepartmentView.as_view(), name='update_department'),
]
