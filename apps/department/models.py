from django.db import models


class Department(models.Model):
    name = models.CharField(max_length=100, blank=True, verbose_name='Department')

    class Meta:
        verbose_name = "Department"
        verbose_name_plural = "Departments"

    def __str__(self):
        return self.name