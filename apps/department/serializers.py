from rest_framework import serializers
from .models import Department


class DepartmentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Department
        fields = (
            'id',
            'name',
        )
        read_only_fields = [
            'id',
        ]

    def create(self, validated_data):

        if Department.objects.filter(name=validated_data['name']).exists():
            raise serializers.ValidationError(
                {'department': 'Department name already exists'})

        return Department.objects.create(**validated_data)


class DepartmentUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = (
            'id',
            'name',
        )
        read_only_fields = [
            'id',
        ]