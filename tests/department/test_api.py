import pytest
from django.urls import reverse
from rest_framework import status


pytestmark = pytest.mark.django_db


def test_department_api_create(admin_client):
    department_object = {'name': 'teste_name'}
    url = reverse('api-department:create_department')
    response = admin_client.post(url, data=department_object)

    assert response.status_code == status.HTTP_201_CREATED


def test_department_list(admin_client):
    url = reverse('api-department:list_department')
    response = admin_client.get(url, content_type="application/json")
    assert response.status_code == status.HTTP_200_OK


def test_department_api_detail(admin_client, department):
    url = reverse('api-department:retrieve_department', args=[department.id])
    response = admin_client.get(url)
    assert response.status_code == status.HTTP_200_OK
    assert response.json()['id'] == department.id


def test_department_api_put(admin_client, department):
    url = reverse('api-department:update_department', args=[department.id])
    response = admin_client.put(url, data={'name': 'new_name'})
    assert response.status_code == status.HTTP_200_OK

